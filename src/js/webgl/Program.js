var Program = {
  getShader: function(gl, id) {
    var script = document.getElementById(id);
    if (!script) {
      return null;
    }

    var str = "";
    var k = script.firstChild;
    while (k) {
      if (k.nodeType == 3) {
        str += k.textContent;
      }
      k = k.nextSibling;
    }

    var shader;
    if (script.type == "x-shader/x-fragment") {
      shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if (script.type == "x-shader/x-vertex") {
      shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
      return null;
    }

    gl.shaderSource(shader, str);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
      alert(gl.getShaderInfoLog(shader));
      return null;
    }
    return shader;
  },

  loadGooch: function() {
    var fragmentShader = Program.getShader(gl, "shader-fs-gooch");
    var vertexShader = Program.getShader(gl, "shader-vs-gooch");

    prg = gl.createProgram();
    gl.attachShader(prg, vertexShader);
    gl.attachShader(prg, fragmentShader);
    gl.linkProgram(prg);

    if (!gl.getProgramParameter(prg, gl.LINK_STATUS)) {
      alert("Could not initialise shaders");
    }

    gl.useProgram(prg);

    prg.aVertexPosition = gl.getAttribLocation(prg, "aVertexPosition");
    prg.aVertexNormal = gl.getAttribLocation(prg, "aVertexNormal");

    prg.uPMatrix = gl.getUniformLocation(prg, "uPMatrix");
    prg.uMVMatrix = gl.getUniformLocation(prg, "uMVMatrix");
    prg.uNMatrix = gl.getUniformLocation(prg, "uNMatrix");

    prg.uMaterialDiffuse = gl.getUniformLocation(prg, "uMaterialDiffuse");
    prg.uMaterialAmbient = gl.getUniformLocation(prg, "uMaterialAmbient");
    prg.uMaterialSpecular = gl.getUniformLocation(prg, "uMaterialSpecular");
    prg.uLightAmbient = gl.getUniformLocation(prg, "uLightAmbient");
    prg.uLightDiffuse = gl.getUniformLocation(prg, "uLightDiffuse");
    prg.uLightSpecular = gl.getUniformLocation(prg, "uLightSpecular");
    prg.uLightPosition = gl.getUniformLocation(prg, "uLightPosition");
    prg.uShininess = gl.getUniformLocation(prg, "uShininess");
    prg.uIsTextured = gl.getUniformLocation(prg, "uIsTextured");
    prg.uAlias = gl.getUniformLocation(prg, "uAlias");

    prg.uSamplerCube = gl.getUniformLocation(prg, "uSamplerCube");

    gl.uniform3fv(prg.uLightPosition, [60, 0, -120]);
    gl.uniform4fv(prg.uLightAmbient, [0.15, 0.15, 0.15, 1.0]);
    gl.uniform4fv(prg.uLightDiffuse, [1.0, 1.0, 1.0, 1.0]);
    gl.uniform4fv(prg.uLightSpecular, [1.0, 1.0, 1.0, 1.0]);
    gl.uniform1f(prg.uShininess, 230.0);
  },

  loadUniform: function() {
    var fragmentShader = Program.getShader(gl, "shader-fs-uniform");
    var vertexShader = Program.getShader(gl, "shader-vs-uniform");

    prg = gl.createProgram();
    gl.attachShader(prg, vertexShader);
    gl.attachShader(prg, fragmentShader);
    gl.linkProgram(prg);

    if (!gl.getProgramParameter(prg, gl.LINK_STATUS)) {
      alert("Could not initialise shaders");
    }

    gl.useProgram(prg);

    prg.aVertexPosition = gl.getAttribLocation(prg, "aVertexPosition");

    prg.uPMatrix = gl.getUniformLocation(prg, "uPMatrix");
    prg.uMVMatrix = gl.getUniformLocation(prg, "uMVMatrix");

    prg.uColor = gl.getUniformLocation(prg, "uColor");
  },

  loadVertexColors: function() {
    var fragmentShader = Program.getShader(gl, "shader-fs-vertexColors");
    var vertexShader = Program.getShader(gl, "shader-vs-vertexColors");

    prg = gl.createProgram();
    gl.attachShader(prg, vertexShader);
    gl.attachShader(prg, fragmentShader);
    gl.linkProgram(prg);

    if (!gl.getProgramParameter(prg, gl.LINK_STATUS)) {
      alert("Could not initialise shaders");
    }

    gl.useProgram(prg);

    prg.aVertexPosition = gl.getAttribLocation(prg, "aVertexPosition");
    prg.aVertexColor = gl.getAttribLocation(prg, "aVertexColor");

    prg.uPMatrix = gl.getUniformLocation(prg, "uPMatrix");
    prg.uMVMatrix = gl.getUniformLocation(prg, "uMVMatrix");
  },

  loadPhong: function() {
    var fragmentShader = Program.getShader(gl, "shader-fs-phong");
    var vertexShader = Program.getShader(gl, "shader-vs-phong");

    prg = gl.createProgram();
    gl.attachShader(prg, vertexShader);
    gl.attachShader(prg, fragmentShader);
    gl.linkProgram(prg);

    if (!gl.getProgramParameter(prg, gl.LINK_STATUS)) {
      alert("Could not initialise shaders");
    }

    gl.useProgram(prg);

    prg.aVertexPosition = gl.getAttribLocation(prg, "aVertexPosition");
    prg.aVertexNormal = gl.getAttribLocation(prg, "aVertexNormal");

    prg.uPMatrix = gl.getUniformLocation(prg, "uPMatrix");
    prg.uMVMatrix = gl.getUniformLocation(prg, "uMVMatrix");
    prg.uNMatrix = gl.getUniformLocation(prg, "uNMatrix");
  }
};
