function Camera() {
  this.matrix = mat4.create();
  this.up = vec3.create();
  this.right = vec3.create();
  this.normal = vec3.create();
  this.position = vec3.create();
  this.focus = vec3.create();
  this.azimuth = 0.0;
  this.elevation = 0.0;
  this.steps = 0;

  this.home = vec3.create();
}

Camera.prototype.goHome = function(h) {
  if (h != null) {
    this.home = h;
  }

  this.setPosition(this.home);
  this.setAzimuth(0);
  this.setElevation(0);
  this.steps = 0;
};

Camera.prototype.dolly = function(s) {
  var c = this;

  var p = vec3.create();
  var n = vec3.create();

  p = c.position;

  var step = s - c.steps;

  vec3.normalize(c.normal, n);

  var newPosition = vec3.create();

  newPosition[0] = p[0];
  newPosition[1] = p[1];
  newPosition[2] = p[2] - step;

  c.setPosition(newPosition);
  c.steps = s;
};

Camera.prototype.setPosition = function(p) {
  vec3.set(p, this.position);
  this.update();
};

Camera.prototype.setFocus = function(f) {
  vec3.set(f, this.focus);
  this.update();
};

Camera.prototype.setAzimuth = function(az) {
  this.changeAzimuth(az - this.azimuth);
};

Camera.prototype.changeAzimuth = function(az) {
  var c = this;
  c.azimuth += az;

  if (c.azimuth > 360 || c.azimuth < -360) {
    c.azimuth = c.azimuth % 360;
  }
  c.update();
};

Camera.prototype.setElevation = function(el) {
  this.changeElevation(el - this.elevation);
};

Camera.prototype.changeElevation = function(el) {
  var c = this;

  c.elevation += el;

  if (c.elevation > 360 || c.elevation < -360) {
    c.elevation = c.elevation % 360;
  }
  c.update();
};

Camera.prototype.update = function() {
  mat4.identity(this.matrix);
  var trxLook = mat4.create();
  mat4.rotateY(this.matrix, (this.azimuth * Math.PI) / 180);
  mat4.rotateX(this.matrix, (this.elevation * Math.PI) / 180);
  mat4.translate(this.matrix, this.position);
};

Camera.prototype.getViewTransform = function() {
  var m = mat4.create();
  mat4.inverse(this.matrix, m);
  return m;
};
