Scene = function Scene() {
  this.object;
  this.vbo;
  this.ibo;
  this.tbo;
  this.indices;
  this.tfbo;
  this.textures = [];
  this.renderbuffer;
  this.mode = "Gooch";
  this.color = [1.0, 0.0, 0.0, 1.0];
  this.vertexColors = false;
};

Scene.prototype.constructor = Scene;

Scene.prototype.addTexture = function(image) {
  this.textures.push(gl.createTexture());
  gl.bindTexture(gl.TEXTURE_2D, this.textures[this.textures.length - 1]);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

  gl.bindTexture(gl.TEXTURE_2D, null);
};

Scene.prototype.loadImage = function(url, callback) {
  var image = new Image();
  image.src = url;
  image.onload = function() {
    callback(image);
  };
};

Scene.prototype.addTriangle = function() {
  const v = [0, 0, 0, 0, 1, 0, 1, 0, 0];
  const i = [0, 1, 2];
  const n = [0, 0, -1, 0, 0, -1, 0, 0, -1];
  const c = [1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1];

  const obj = { vertices: v, indices: i, normals: n, vertexColors: c };
  this.addObject(obj);
};

Scene.prototype.addObject = function(object) {
  object.vbo = gl.createBuffer();
  object.ibo = gl.createBuffer();
  object.nbo = gl.createBuffer();

  gl.bindBuffer(gl.ARRAY_BUFFER, object.vbo);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(object.vertices), gl.STATIC_DRAW);

  if (object.normals) {
    gl.bindBuffer(gl.ARRAY_BUFFER, object.nbo);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(object.normals), gl.STATIC_DRAW);
  }

  if (object.uvMaps) {
    object.tbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, object.tbo);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(object.uvMaps[0].uv), gl.STATIC_DRAW);
  }

  if (object.vertexColors) {
    object.cbo = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, object.cbo);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(object.vertexColors), gl.STATIC_DRAW);
  }

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, object.ibo);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(object.indices), gl.STATIC_DRAW);

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
  gl.bindBuffer(gl.ARRAY_BUFFER, null);

  this.object = object;
};

Scene.prototype.draw = function() {
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  gl.enable(gl.DEPTH_TEST);

  Program.loadUniform();
  gl.uniform4fv(prg.uColor, this.color);

  transforms.updatePerspective();

  try {
    var object = this.object;
    transforms.calculateModelView();
    transforms.setMatrixUniforms();

    //vertex buffer
    gl.bindBuffer(gl.ARRAY_BUFFER, object.vbo);
    gl.enableVertexAttribArray(prg.aVertexPosition);
    gl.vertexAttribPointer(prg.aVertexPosition, 3, gl.FLOAT, false, 0, 0);

    //normal buffer
    // if(object.normals){
    //    gl.bindBuffer(gl.ARRAY_BUFFER, object.nbo);
    //    gl.enableVertexAttribArray(prg.aVertexNormal);
    //    gl.vertexAttribPointer(prg.aVertexNormal, 3, gl.FLOAT, false, 0, 0);
    // }

    if (object.vertexColors) {
      gl.bindBuffer(gl.ARRAY_BUFFER, object.cbo);
      gl.enableVertexAttribArray(prg.aVertexColor);
      gl.vertexAttribPointer(prg.aVertexColor, 4, gl.FLOAT, false, 0, 0);
    }

    //index buffer
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, object.ibo);

    gl.drawElements(gl.TRIANGLES, object.indices.length, gl.UNSIGNED_SHORT, 0);

    //unbinding buffers
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
  } catch (err) {
    //creative bugfixing
  }
};

Scene.prototype.setShadingMode = function(mode) {
  this.mode = mode;
};

Scene.prototype.setColor = function(color) {
  this.color = color;
};

Scene.prototype.loadCTM = function(filename) {
  request = new XMLHttpRequest();
  request.open("GET", filename, true);
  request.overrideMimeType("text/plain; charset=x-user-defined");

  request.onreadystatechange = function() {
    if (request.readyState == 4) {
      if (request.status == 404) {
        console.info(filename + " does not exist");
      }
    } else {
      if (request.responseText != "") {
        var stream = new CTM.Stream(request.responseText);
        var file = new CTM.File(stream);
        scene.addObject(file.body);
      }
    }
  };
  request.send();
};

//TODO load an obj file and return a new object with indices and vertices.
//hint: you can get the text baeśed data with  request.responseText --> look at the loadCTM
//function on how to get a request.
Scene.prototype.loadOBJ = function(filename) {};

//use regex to extract the vertex information from the data
Scene.prototype.getVertices = function(data) {};

//use regex to extract the normal information from the data
Scene.prototype.getNormals = function(data) {};

//use regex to extract the index information from the data. hint: obj indices start with 1,
//but webgl expects them to start with 0
Scene.prototype.getIndices = function(data) {};
